import { ThemeProvider } from '@emotion/react';
import { Container, createTheme, CssBaseline, Paper } from '@mui/material';
import { useState } from 'react';
import QuestionnaireComponent from './components/QuestionnaireComponent/QuestionnaireComponent';
import { Questionnaire } from './questionnaire/Questionnaire';
import { QuestionnaireFactory } from './questionnaire/QuestionnaireFactory';

function App() {
	const theme = createTheme();
	const [questionnaire] = useState<Questionnaire>(
		QuestionnaireFactory.createCarbonaraQuestionnaire()
	);

	return (
		<ThemeProvider theme={theme}>
			<CssBaseline />
			<Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
				<Paper
					variant="outlined"
					sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
				>
					<QuestionnaireComponent questionnaire={questionnaire} />
				</Paper>
			</Container>
		</ThemeProvider>
	);
}

export default App;
