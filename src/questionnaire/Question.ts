export class Question {
	public title: string;
	public question: string;
	public answers: string[];
	public correct: number;

	public constructor(
		title: string,
		question: string,
		answers: string[],
		correct: number
	) {
		this.title = title;
		this.question = question;
		this.answers = answers;
		this.correct = correct;
	}
}
