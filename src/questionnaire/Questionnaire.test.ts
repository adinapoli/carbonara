import '@testing-library/jest-dom/extend-expect';
import { QuestionnaireFactory } from './QuestionnaireFactory';

describe('Carbonara questionnaire test', () => {
	test('it should have 0 correct answers', () => {
		const carbonaraQuestionnaire =
			QuestionnaireFactory.createCarbonaraQuestionnaire();
		const answers = new Map<string, number>();
		answers.set(carbonaraQuestionnaire.questions[0].question, 1);
		answers.set(carbonaraQuestionnaire.questions[1].question, 0);
		answers.set(carbonaraQuestionnaire.questions[2].question, 0);

		expect(carbonaraQuestionnaire.getScore(answers)).toBe(0);
		expect(carbonaraQuestionnaire.getResult(answers)).toBe(
			'Non sai fare la carbonara'
		);
	});

	test('it should have 1 correct answers', () => {
		const carbonaraQuestionnaire =
			QuestionnaireFactory.createCarbonaraQuestionnaire();
		const answers = new Map<string, number>();
		answers.set(carbonaraQuestionnaire.questions[0].question, 1);
		answers.set(carbonaraQuestionnaire.questions[1].question, 1);
		answers.set(carbonaraQuestionnaire.questions[2].question, 0);

		expect(carbonaraQuestionnaire.getScore(answers)).toBe(1);
		expect(carbonaraQuestionnaire.getResult(answers)).toBe('Non ci siamo');
	});

	test('it should have 2 correct answers', () => {
		const carbonaraQuestionnaire =
			QuestionnaireFactory.createCarbonaraQuestionnaire();
		const answers = new Map<string, number>();
		answers.set(carbonaraQuestionnaire.questions[0].question, 0);
		answers.set(carbonaraQuestionnaire.questions[1].question, 1);
		answers.set(carbonaraQuestionnaire.questions[2].question, 0);

		expect(carbonaraQuestionnaire.getScore(answers)).toBe(2);
		expect(carbonaraQuestionnaire.getResult(answers)).toBe(
			'Devi studiare di più'
		);
	});

	test('it should have 3 correct answers', () => {
		const carbonaraQuestionnaire =
			QuestionnaireFactory.createCarbonaraQuestionnaire();
		const answers = new Map<string, number>();
		answers.set(carbonaraQuestionnaire.questions[0].question, 0);
		answers.set(carbonaraQuestionnaire.questions[1].question, 1);
		answers.set(carbonaraQuestionnaire.questions[2].question, 1);

		expect(carbonaraQuestionnaire.getScore(answers)).toBe(3);
		expect(carbonaraQuestionnaire.getResult(answers)).toBe('Bravo');
	});
});
