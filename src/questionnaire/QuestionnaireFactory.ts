import { Question } from './Question';
import { Questionnaire } from './Questionnaire';

// TODO it should be nice if data is read directly from json
export class QuestionnaireFactory {
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	private constructor() {}

	public static createCarbonaraQuestionnaire() {
		const formaggioQuestion = new Question(
			'Formaggio',
			'Che formaggio useresti?',
			['Pecorino', 'Grana', 'Gruviera'],
			0
		);

		const uovaQuestion = new Question(
			'Uova',
			'Quante uova metti nel sugo?',
			['1 uovo per persona', '1 rosso per persona', '2 uova a testa'],
			1
		);

		const insaccatoQuestion = new Question(
			'Insaccato',
			'Che insaccato utilizzi?',
			['Pancetta', 'Guanciale', 'Prosciutto'],
			1
		);

		const resultsByScore = new Map<number, string>([
			[0, 'Non sai fare la carbonara'],
			[1, 'Non ci siamo'],
			[2, 'Devi studiare di più'],
			[3, 'Bravo'],
		]);

		return new Questionnaire(
			'Carbonara',
			[formaggioQuestion, uovaQuestion, insaccatoQuestion],
			resultsByScore
		);
	}
}
