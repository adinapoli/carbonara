import { Question } from './Question';

export class Questionnaire {
	public title: string;
	public questions: Question[];
	private resultsByScore: Map<number, string>;

	public constructor(
		title: string,
		questions: Question[],
		resultsByScore: Map<number, string>
	) {
		this.title = title;
		this.questions = questions;
		this.resultsByScore = resultsByScore;
	}

	public getScore(answers: Map<string, number>) {
		let score = 0;
		for (const question of this.questions) {
			if (question.correct === answers.get(question.question)) score++;
		}
		return score;
	}

	public getResult(answers: Map<string, number>) {
		const score = this.getScore(answers);
		return this.resultsByScore.get(score);
	}
}
