import { FormControl, Grid, MenuItem, Select, Typography } from '@mui/material';
import React, { FC, useEffect, useState } from 'react';
import { Question } from '../../questionnaire/Question';

interface QuestionComponentProps {
	question: Question;
	handleSelectAnswer: (question: string, answer: number) => void;
}

const QuestionComponent: FC<QuestionComponentProps> = (
	props: QuestionComponentProps
) => {
	const defaultAnswer = 0;
	const [answer, setAnswer] = useState(defaultAnswer);

	useEffect(() => {
		setAnswer(defaultAnswer);
		props.handleSelectAnswer(props.question.question, defaultAnswer);
	}, [props.question]);

	return (
		<React.Fragment>
			<Typography variant="h6" gutterBottom>
				{props.question.question}
			</Typography>
			<Grid container spacing={3}>
				<Grid item xs={12} sm={6}>
					<FormControl fullWidth>
						<Select
							labelId="answer-label"
							id="answer"
							value={answer}
							label="Answer"
							onChange={(event) => {
								const answer = event.target.value as number;
								setAnswer(answer);
								props.handleSelectAnswer(props.question.question, answer);
							}}
						>
							{props.question.answers.map((answer, index) => (
								<MenuItem value={index} key={index}>
									{answer}
								</MenuItem>
							))}
							;
						</Select>
					</FormControl>
				</Grid>
			</Grid>
		</React.Fragment>
	);
};

export default QuestionComponent;
