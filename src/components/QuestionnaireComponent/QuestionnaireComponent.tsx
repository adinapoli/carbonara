import {
	Box,
	Button,
	Step,
	StepLabel,
	Stepper,
	Typography,
} from '@mui/material';
import React, { FC, useEffect, useState } from 'react';
import { Question } from '../../questionnaire/Question';
import { Questionnaire } from '../../questionnaire/Questionnaire';
import QuestionComponent from '../QuestionComponent/QuestionComponent';

interface QuestionnaireComponentProps {
	questionnaire: Questionnaire;
}

const QuestionnaireComponent: FC<QuestionnaireComponentProps> = (
	props: QuestionnaireComponentProps
) => {
	const steps = props.questionnaire.questions.map((question) => question.title);

	const [activeStep, setActiveStep] = useState(0);
	const [question, setQuestion] = useState<Question>(
		props.questionnaire.questions[0]
	);

	const [answers, setsAnswers] = useState<Map<string, number>>(
		new Map<string, number>()
	);
	const handleSelectAnswer = (question: string, answer: number) => {
		setsAnswers(new Map(answers).set(question, answer));
	};

	const handleNext = () => {
		setActiveStep(activeStep + 1);
	};

	useEffect(() => {
		setQuestion(props.questionnaire.questions[activeStep]);
	}, [activeStep]);

	return (
		<React.Fragment>
			<Typography component="h1" variant="h4" align="center">
				{props.questionnaire.title}
			</Typography>

			<Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
				{steps.map((label) => (
					<Step key={label}>
						<StepLabel>{label}</StepLabel>
					</Step>
				))}
			</Stepper>

			<React.Fragment>
				{activeStep === steps.length ? (
					<React.Fragment>
						<Typography variant="h5" gutterBottom>
							Hai risposto correttamente a{' '}
							{props.questionnaire.getScore(answers)} su{' '}
							{props.questionnaire.questions.length}
						</Typography>
						<Typography variant="subtitle1">
							{props.questionnaire.getResult(answers)}
						</Typography>
					</React.Fragment>
				) : (
					<React.Fragment>
						{question && (
							<QuestionComponent
								question={question}
								handleSelectAnswer={handleSelectAnswer}
							/>
						)}
						<Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
							<Button
								variant="contained"
								onClick={handleNext}
								sx={{ mt: 3, ml: 1 }}
							>
								{activeStep === steps.length - 1 ? 'Risultato' : 'Avanti'}
							</Button>
						</Box>
					</React.Fragment>
				)}
			</React.Fragment>
		</React.Fragment>
	);
};

export default QuestionnaireComponent;
